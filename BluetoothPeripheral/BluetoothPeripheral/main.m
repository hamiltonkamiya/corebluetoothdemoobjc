//
//  main.m
//  BluetoothPeripheral
//
//  Created by macOS on 14/03/20.
//  Copyright © 2020 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
