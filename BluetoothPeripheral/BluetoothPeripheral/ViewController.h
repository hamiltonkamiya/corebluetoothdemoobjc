//
//  ViewController.h
//  BluetoothPeripheral
//
//  Created by macOS on 14/03/20.
//  Copyright © 2020 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface ViewController : UIViewController <CBPeripheralManagerDelegate>

@property(strong, nonatomic)CBPeripheralManager *peripheral;
@property(strong, nonatomic)NSMutableData *data;

@end

