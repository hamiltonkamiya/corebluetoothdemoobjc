//
//  InterfaceController.h
//  BluetoothPeripheral WatchKit Extension
//
//  Created by macOS on 14/03/20.
//  Copyright © 2020 macOS. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>

@interface InterfaceController : WKInterfaceController

@end
