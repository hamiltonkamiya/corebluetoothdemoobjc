//
//  ViewController.h
//  BluetoothCentral
//
//  Created by macOS on 16/03/20.
//  Copyright © 2020 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

@protocol CentralProtocol <NSObject>

-(void)sampleProtocol;

@end
@interface ViewController : UIViewController<CBCentralManagerDelegate, CBPeripheralDelegate, CentralProtocol>

@property (nonatomic, weak) id<CentralProtocol> delegate;

-(void)showAlert;
-(void)scan;
-(void)makeBlock;

@end
